// 2. В классе «User» реализуйте сл. свойства и методы:
// 1. Свойство data – для хранения данных о контакте в виде простого объекта со сл. полями: id,
// name, email, address, phone.
// 2. Метод edit(obj) – для редактирования данных контакта. В качестве параметра метод должен
// принимать объект с обновленными данными и обновлять свойство data.
// 3. Метод get() – для получения данных о контакте. Метод должен возвращать объект с данными
// из св-ва data.
// 4. При создании объекта на основе этого класса важно передать в конструктор данные о контакте
// в виде объекта для дальнейшего сохранения в св-во data.

class User {
    #data = {}
    constructor({ id, name, email, address, phone }) {
        this.#data = {
            id,
            name,
            email,
            address,
            phone,    
        }
    }

    set userData (newData) {
        this.#data = {...newData}
    }

    get userData () {
        return this.#data
    }
}

// 3. В классе «Contacts» реализуйте сл. св-ва и методы:
// 1. Свойство data – для хранения всех добавленных контактов в массиве. Каждый эл-т массива
// должен представлять собой объект созданный на основе класса «User».
// 2. Метод add() – для создания контакта (на основе класса «User») и добавления его в массив data.
// 3. Метод edit(id, obj) – для редактирования информации конкретного контакты из св-ва data,
// используя соответствующий метод из «User». В качестве параметра нужно передать
// идентификатор контакта для последующего поиска и объект с новыми данными для
// редактирования.
// 4. Метод remove(id) – для удаления контакта из общего массива данных по идентификатору. В
// качестве параметра нужно передать идентификатор контакта.
// 5. Метод get() – для получения


class Contacts {
    constructor() {
        this.contactsData = [];
    }

    add ({ name, phone, address, email }) {
        const contactUser = new User({
            id: new Date().getTime().toString(),
            name,
            phone,
            address,
            email
        })
        this.contactsData.push(contactUser)
    }

    edit(userId, updateUserData) {
        this.contactsData = this.contactsData.map((user)=>{
            if(user.userData.id === userId) {
                user.userData = {
                    id: userId,
                    ...updateUserData,
                }
                // user.userData = {
                //     ...user['userData'],
                //     ...updateUserData,
                // }
            }
            return user
        })
    }

    remove (userId) {
        this.contactsData = this.contactsData.filter((user)=> user.userData.id !== userId)
    }

    get () {
        return this.contactsData
    }
}
// В классе «ContactsApp» реализуйте сл. св-ва и методы:
// 1. При создании объекта на основе этого класса в DOM должен добавляться главный контейнер
// приложения, например, «<div class=”contacts”></div>». Доступ к контейнеру должен быть
// через свойство «app» (должен хранить созданный элемент).
// 2. Также, в конструкторе или через любой метод в классе полностью создайте интерфейс вашего
// приложения внутри главного контейнера. Предусмотрите форму с полями и кнопками для
// добавления и редактирования контактов. Дизайн может быть любым, но адаптивным к
// мобильным устройствам.
// 3. Методы onAdd(), onEdit() и onRemove() – должны срабатывать по клику по соотв. кнопкам в
// интерфейсе для добавления/редактирования/удаления контакта. Важно использование
// методов от «Contacts» при соотв. действиях.
// 4. Метод get() - для получения и обновления списка контактов в соотв. контейнере вашего
// приложения. Важно сохранить возможности родительского метода.

class ContactsApp extends Contacts {
    #inputName;
    #inputTel;
    #inputEmail;
    #inputAddress;


    constructor(){
        super();
        this.checkCookieForLocalStorage();
        this.contactsData = this.loadedLocalStorageData() || [];
        this.create();
        this.addEventForButtonAdd();
        this.getData();

    }

    async getData () {
        let url = 'https://jsonplaceholder.typicode.com/users';
        if(localStorage.length !== 0 ) return;
        let response = await fetch (url);
        if (!response.ok) {
            return;
        }
        let data = await response.json();

        const newData = data.map((item) => {
            let user = new User ({
                id: item.id,
                name: item.name,
                phone: item.phone,
                email: item.email,
                address: item.address.street,
            });
            return user;
        });

        // newData.forEach((item) => {
        //     this.add(item);
        // })

        this.contactsData = [...newData];
        console.log(this.contactsData);
        this.render();
        //return this.contactsData;    
    };

    loadedLocalStorageData() {
        const localStorageData = JSON.parse(this.storage);
        if(!localStorageData) {
            return;
        }
        const data = localStorageData.map(item => new User(item));
        return data;
    }

    get storage () {
        return localStorage.getItem('contact');
    }

    set storage (data) {
        const contacts = data.map (item => item.userData); 
        this.storageExpiration();
        localStorage.setItem('contact', JSON.stringify(contacts));
    }

    findCookie(name) {
        let matches = document.cookie.match(new RegExp(
              "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    checkCookieForLocalStorage() {
        if(!this.findCookie('user')) {
            localStorage.clear();
        }
    }

    storageExpiration(){
        document.cookie = 'user=contacts; max-age=60';
    }   

    addEventForButtonAdd() {
        const addButton = document.querySelector('.contact__button');
        addButton.addEventListener('click',() => {
            const inputName = document.querySelector('.contact__name');
            const inputTel = document.querySelector('.contact__phone');
            const inputEmail = document.querySelector('.contact__email');
            const inputAddress = document.querySelector('.contact__address');
            this.onAdd({
                name: inputName.value,
                phone: inputTel.value,
                email: inputEmail.value,
                address: inputAddress.value,
            })
            inputName.value="";
            inputTel.value="";
            inputEmail.value="";
            inputAddress.value=""
        })
    }

    create() {
        const app = document.querySelector('.app');
        if (!app) {
            return;
        }
        const contactsHtml = this.createHtml();
        app.appendChild(contactsHtml);
        this.render();
    }

    createHtml () {
        const contactsElement = document.createElement('div')
        contactsElement.classList.add('contacts')
        contactsElement.innerHTML= `<div class="contacts__header">
                                        <input type="text" class="contact__name" placeholder="Имя">
                                        <input type="tel" class="contact__phone" placeholder="Номер Телефона">
                                        <input type="email" class="contact__email" placeholder="Email">
                                        <input type="text" class="contact__address" placeholder="Address">
                                        <button class="contact__button">Добавить</button>
                                    </div>
                                    <div class="contacts__content">
                                    </div>`

        return contactsElement
    }

    onAdd(contact) {
        this.add(contact); 
        this.storage = this.contactsData; // сработал сетер
        this.render();
    }

    onRemove(contactId) {
        this.remove(contactId);
        this.storage = this.contactsData;
        this.render();
    }
    
    addEventDeleteButtons() {
        const deleteBtns = document.querySelectorAll('.delete__button');
        deleteBtns.forEach((deleteBtn) => {
            deleteBtn.addEventListener('click', (e) => {
                this.onRemove(e.target.id);
            })
        })
    }

    render() {
        const contentElement = document.querySelector('.contacts__content');
        contentElement.innerHTML = "";

        if(this.contactsData.length === 0) {
            contentElement.innerHTML = `<h3 class = "content__empty">Нет данных</h3>`
            return;
        }

        const contactItemList = document.createElement('ul');
        contactItemList.classList.add('contacts__items');

        let contactItems = '';

        this.contactsData.forEach((item)=> {
            const data = item.userData;
            const {name, address, phone, id, email} = data;
            contactItems += `<li>
                                <div class = "name">Имя: ${name}</div>
                                <div class = "phone">Телефон: ${phone}</div>
                                <div class = "email">Email: ${email}</div>
                                <div class = "address">Адрес: ${address}</div>
                                <button class = "delete__button" id = "${id}">Удалить</button>
                            </li>`
        })

        contactItemList.innerHTML = contactItems;

        contentElement.appendChild(contactItemList);

        this.addEventDeleteButtons();

    }

}

const app = new ContactsApp()


